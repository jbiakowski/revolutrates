object Versions {
    const val KOTLIN = "1.3.21"

    const val SUPPORT = "28.0.0"

    const val RX_JAVA = "2.2.7"
    const val RETROFIT = "2.5.0"
    const val DAGGER = "2.13"
    const val OK_HTTP = "3.3.1"
    const val MOSHI = "1.8.0"
    const val RX_ANDROID = "2.1.1"
    const val RX_KOTLIN = "2.3.0"
    const val STETHO = "1.5.0"
    const val RX_BINDING = "3.0.0-alpha2"
}

object Dependencies {
    const val KOTLIN_PLUGIN = "org.jetbrains.kotlin:kotlin-gradle-plugin:${Versions.KOTLIN}"

    const val KOTLIN = "org.jetbrains.kotlin:kotlin-stdlib-jdk8:${Versions.KOTLIN}"

    const val APPCOMPAT = "com.android.support:appcompat-v7:${Versions.SUPPORT}"
    const val RECYCLER_VIEW = "com.android.support:recyclerview-v7:${Versions.SUPPORT}"

    const val OK_HTTP = "com.squareup.okhttp3:okhttp:${Versions.OK_HTTP}"
    const val DAGGER = "com.google.dagger:dagger:${Versions.DAGGER}"
    const val DAGGER_COMPILER = "com.google.dagger:dagger-compiler:${Versions.DAGGER}"
    const val MOSHI = "com.squareup.moshi:moshi:${Versions.MOSHI}"
    const val MOSHI_ADAPTERS = "com.squareup.moshi:moshi-adapters:${Versions.MOSHI}"
    const val MOSHI_KOTLIN = "com.squareup.moshi:moshi-kotlin:${Versions.MOSHI}"
    const val RETROFIT = "com.squareup.retrofit2:retrofit:${Versions.RETROFIT}"
    const val RETROFIT_RXJAVA = "com.squareup.retrofit2:adapter-rxjava2:${Versions.RETROFIT}"
    const val RETROFIT_MOSHI = "com.squareup.retrofit2:converter-moshi:${Versions.RETROFIT}"
    const val RX_JAVA = "io.reactivex.rxjava2:rxjava:${Versions.RX_JAVA}"
    const val RX_ANDROID = "io.reactivex.rxjava2:rxandroid:${Versions.RX_ANDROID}"
    const val RX_KOTLIN = "io.reactivex.rxjava2:rxkotlin:${Versions.RX_KOTLIN}"
    const val RX_BINDING = "com.jakewharton.rxbinding3:rxbinding:${Versions.RX_BINDING}"
    const val STETHO = "com.facebook.stetho:stetho:${Versions.STETHO}"
    const val STETHO_OK_HTTP = "com.facebook.stetho:stetho-okhttp3:${Versions.STETHO}"
}