package pl.whiter13.revolut.rates

import dagger.Module
import dagger.Provides
import dagger.Subcomponent
import pl.whiter13.revolut.RevolutApplication
import pl.whiter13.revolut.rates.api.RatesApi
import pl.whiter13.revolut.rates.ui.RatesFragment
import retrofit2.Retrofit
import javax.inject.Scope

@MustBeDocumented
@Scope
@Retention(AnnotationRetention.RUNTIME)
internal annotation class RatesScope

@RatesScope
@Subcomponent(modules = [RatesModule::class])
internal interface RatesComponent {

    fun inject(ratesFragment: RatesFragment)

    companion object {
        fun get() = RevolutApplication.applicationComponent
            .plus(RatesModule())
    }
}

@Module
internal class RatesModule {

    @RatesScope
    @Provides
    fun provideRatesPresenter(ratesInteractor: RatesInteractor) = RatesPresenter(ratesInteractor)

    @RatesScope
    @Provides
    fun provideRatesInteractor(ratesApi: RatesApi, rateExchangeCalculator: RateExchangeCalculator): RatesInteractor {
        return RatesInteractor(ratesApi = ratesApi, rateExchangeCalculator = rateExchangeCalculator)
    }

    @RatesScope
    @Provides
    fun provideRatesApi(retrofit: Retrofit): RatesApi = retrofit.create(RatesApi::class.java)

    @RatesScope
    @Provides
    fun provideRateExchangeCalculator(): RateExchangeCalculator = RateExchangeCalculator()
}