package pl.whiter13.revolut.rates.ui

import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import com.jakewharton.rxbinding3.widget.TextViewAfterTextChangeEvent
import com.jakewharton.rxbinding3.widget.afterTextChangeEvents
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.Subject
import pl.whiter13.revolut.R
import pl.whiter13.revolut.rates.ViewEvent
import pl.whiter13.revolut.rates.model.Rate
import java.math.BigDecimal
import java.util.concurrent.TimeUnit

internal typealias EditableValueStateInterceptor = (String?) -> Unit

internal class RatesAdapter : RecyclerView.Adapter<RateViewHolder>() {

    private val rates: MutableList<Rate> = mutableListOf()
    private val editableValueListener = BehaviorSubject.create<ViewEvent.ActiveRateInput>()
    private val rateSelectionListener = BehaviorSubject.create<ViewEvent.RateSelection>()

    private var lastEditableValueState: String? = null

    private val editableValueStateInterceptor: EditableValueStateInterceptor = {
        lastEditableValueState = it
    }

    private lateinit var editableRate: Rate

    fun update(rates: List<Rate>, editableRateName: String) {
        this.rates.run {
            clear()
            addAll(rates)
        }
        lastEditableValueState = null
        editableRate = rates.find { it.name == editableRateName }!!
        notifyDataSetChanged()
    }

    fun editableValue(): Observable<ViewEvent.ActiveRateInput> = editableValueListener

    fun rateSelection(): Observable<ViewEvent.RateSelection> = rateSelectionListener

    override fun onCreateViewHolder(container: ViewGroup, viewType: Int): RateViewHolder {
        return LayoutInflater
            .from(container.context)
            .inflate(R.layout.item_rate, container, false)
            .let { RateViewHolder(it, editableValueStateInterceptor) }
    }

    override fun getItemCount(): Int = rates.size

    override fun onBindViewHolder(viewHolder: RateViewHolder, index: Int) {
        rates[index].let {
            viewHolder.bind(
                rate = it,
                editableValuePredicate = { rate -> rate.name == editableRate.name },
                editableValueListener = editableValueListener,
                lastEditableValue = lastEditableValueState,
                selectionListener = rateSelectionListener
            )
        }
    }

    override fun onViewAttachedToWindow(holder: RateViewHolder) = holder.onAttach()

    override fun onViewDetachedFromWindow(holder: RateViewHolder) = holder.onDetach()
}

internal class RateViewHolder(
    private val view: View,
    private val editableValueStateInterceptor: EditableValueStateInterceptor
) : RecyclerView.ViewHolder(view) {

    private val name = view.findViewById<TextView>(R.id.item_rate_name)
    private val value = view.findViewById<EditText>(R.id.item_rate_value)

    private val disposables = CompositeDisposable()

    private lateinit var rate: Rate
    private lateinit var editableValuePredicate: (Rate) -> Boolean
    private lateinit var editableValueListener: Subject<ViewEvent.ActiveRateInput>

    fun bind(
        rate: Rate,
        editableValuePredicate: (Rate) -> Boolean,
        editableValueListener: Subject<ViewEvent.ActiveRateInput>,
        lastEditableValue: String?,
        selectionListener: Subject<ViewEvent.RateSelection>
    ) {
        this.rate = rate
        this.editableValuePredicate = editableValuePredicate
        this.editableValueListener = editableValueListener

        name.text = rate.name

        if (editableValuePredicate(rate)) {
            val valueToSet = lastEditableValue ?: rate.defaultValue
            value.setText(valueToSet)

            view.isClickable = false
            view.setOnClickListener(null)
        } else {
            view.isClickable = true
            view.setOnClickListener {
                selectionListener.onNext(rate.toRateSelection())
            }
        }
    }

    fun onAttach() {
        val editable = editableValuePredicate(rate)
        value.isEnabled = editable

        rate.exchangeRate
            .distinctUntilChanged()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { value.setText(it.toPlainString()) }
            .addTo(disposables)

        if (editable) {
            listenToChanges(editableValueListener)
        }
    }

    fun onDetach() {
        if (editableValuePredicate(rate)) {
            editableValueStateInterceptor(value.text.toString())
        }
        disposables.clear()
    }

    private fun listenToChanges(editableValueListener: Subject<ViewEvent.ActiveRateInput>) {
        value.afterTextChangeEvents()
            .debounce(200, TimeUnit.MILLISECONDS)
            .subscribe { editableValueListener.onNext(it.toActiveRateInput()) }
            .addTo(disposables)
    }

    private fun TextViewAfterTextChangeEvent.toActiveRateInput(): ViewEvent.ActiveRateInput {
        return ViewEvent.ActiveRateInput(
            rate = rate.name,
            value = editable.toBigDecimalOrNull()
        )
    }

    private fun Rate.toRateSelection(): ViewEvent.RateSelection {
        return ViewEvent.RateSelection(
            rate = name,
            value = value.text.toBigDecimalOrNull()
        )
    }

    private fun Editable?.toBigDecimalOrNull(): BigDecimal? {
        return toString().toBigDecimalOrNull()
    }
}
