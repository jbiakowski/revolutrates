package pl.whiter13.revolut.rates.ui

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.reactivex.Observable
import kotlinx.android.synthetic.main.fragment_rates.*
import pl.whiter13.revolut.R
import pl.whiter13.revolut.rates.RatesComponent
import pl.whiter13.revolut.rates.RatesPresenter
import pl.whiter13.revolut.rates.RatesView
import pl.whiter13.revolut.rates.ViewEvent
import pl.whiter13.revolut.rates.model.Rate
import javax.inject.Inject

internal class RatesFragment : Fragment(), RatesView {

    @Inject
    lateinit var ratesPresenter: RatesPresenter

    private lateinit var ratesAdapter: RatesAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_rates, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ratesAdapter = RatesAdapter()
        val orientation = LinearLayoutManager.VERTICAL

        fragment_rates_recycler_view.run {
            adapter = ratesAdapter
            layoutManager = LinearLayoutManager(context, orientation, false)
            addItemDecoration(DividerItemDecoration(context, orientation))
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        RatesComponent.get().inject(this)
    }

    override fun onResume() {
        super.onResume()
        ratesPresenter.attachView(this)
    }

    override fun onPause() {
        super.onPause()
        ratesPresenter.detachView()
    }

    override fun showError() {
        fragment_rates_error_view.visibility = View.VISIBLE
        fragment_rates_recycler_view.visibility = View.GONE
    }

    override fun showRates(rates: List<Rate>, editableRateName: String) {
        fragment_rates_error_view.visibility = View.GONE
        fragment_rates_recycler_view.visibility = View.VISIBLE

        ratesAdapter.update(rates, editableRateName)
        fragment_rates_recycler_view.scrollToPosition(0)
    }

    override fun viewEvents(): Observable<out ViewEvent> {
        return Observable.merge(listOf(ratesAdapter.editableValue(), ratesAdapter.rateSelection()))
    }
}