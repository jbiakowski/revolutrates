package pl.whiter13.revolut.rates.api

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

internal interface RatesApi {

    @GET("latest")
    fun rates(@Query("base") base: String): Single<RatesResponse>
}