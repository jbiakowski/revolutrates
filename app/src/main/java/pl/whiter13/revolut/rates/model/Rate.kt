package pl.whiter13.revolut.rates.model

import io.reactivex.Flowable
import java.math.BigDecimal

internal data class Rate(
    val name: String,
    val exchangeRate: Flowable<BigDecimal>,
    val defaultValue: String? = null
)