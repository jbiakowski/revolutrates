package pl.whiter13.revolut.rates

import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.processors.BehaviorProcessor
import io.reactivex.processors.FlowableProcessor
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.combineLatest
import pl.whiter13.revolut.rates.api.RatesApi
import pl.whiter13.revolut.rates.api.RatesResponse
import pl.whiter13.revolut.rates.model.Rate
import java.math.BigDecimal
import java.util.concurrent.TimeUnit

internal data class Value(val value: BigDecimal?)

internal class RatesInteractor(
    private val ratesApi: RatesApi,
    private val rateExchangeCalculator: RateExchangeCalculator
) {

    fun rates(
        rate: String,
        valueStream: Observable<Value>,
        firstValue: BigDecimal? = null
    ): Observable<List<Rate>> {
        return Observable.create<List<Rate>> { emitter ->
            val compositeDisposable = CompositeDisposable()

            ratesApi.rates(rate)
                .flatMapPublisher {
                    val keysWithExchangeStream = it.rates.mapValues { BehaviorProcessor.createDefault(BigDecimal.ZERO) }
                    val rates = createRates(keysWithExchangeStream)

                    val finalList = rates.withBaseRate(createBaseRate(rate, firstValue))
                    emitter.onNext(finalList)

                    refreshRates(rate, valueStream, keysWithExchangeStream)
                }
                .subscribe({}, { emitter.onError(it) })
                .addTo(compositeDisposable)

            emitter.setDisposable(compositeDisposable)
        }
    }

    private fun refreshRates(
        rate: String,
        valueStream: Observable<Value>,
        keysWithExchangeStream: Map<String, FlowableProcessor<BigDecimal>>
    ): Flowable<Pair<RatesResponse, Value>>? {
        return Flowable.interval(1, TimeUnit.SECONDS)
            .flatMapSingle { ratesApi.rates(rate) }
            .combineLatest(valueStream.toFlowable(BackpressureStrategy.LATEST))
            .doOnNext { latest ->
                latest.first.rates.forEach { key, value ->
                    keysWithExchangeStream[key]?.run {
                        onNext(rateExchangeCalculator.calculate(baseRateExchange = value, value = latest.second.value))
                    }
                }
            }
    }
}

private fun createBaseRate(rate: String, firstValue: BigDecimal?): Rate {
    val baseExchangeRate = BehaviorProcessor.create<BigDecimal>()
    return Rate(name = rate, exchangeRate = baseExchangeRate, defaultValue = firstValue?.toPlainString())
}

private fun List<Rate>.withBaseRate(baseRate: Rate): List<Rate> {
    return toMutableList().apply { add(0, baseRate) }
}

private fun createRates(keysWithExchangeStream: Map<String, BehaviorProcessor<BigDecimal>>): List<Rate> {
    return keysWithExchangeStream.map { entry -> Rate(name = entry.key, exchangeRate = entry.value) }
}