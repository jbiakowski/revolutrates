package pl.whiter13.revolut.rates

import java.math.BigDecimal

internal class RateExchangeCalculator {

    fun calculate(baseRateExchange: BigDecimal, value: BigDecimal?): BigDecimal {
        return when (value) {
            null -> BigDecimal.ZERO
            else -> baseRateExchange * value
        }
    }
}