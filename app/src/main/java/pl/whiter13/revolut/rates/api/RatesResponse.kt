package pl.whiter13.revolut.rates.api

import com.squareup.moshi.Json
import java.math.BigDecimal
import java.util.*

internal data class RatesResponse(
    @Json(name = "base") val base: String,
    @Json(name = "date") val date: Date,
    @Json(name = "rates") val rates: Map<String, BigDecimal>
)