package pl.whiter13.revolut.rates

import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.subjects.PublishSubject
import pl.whiter13.revolut.rates.model.Rate
import java.math.BigDecimal

internal class RatesPresenter(private val ratesInteractor: RatesInteractor) {

    private val disposables = CompositeDisposable()
    private val valueSubject = PublishSubject.create<Value>()

    private var ratesView: RatesView? = null

    fun attachView(ratesView: RatesView) {
        this.ratesView = ratesView

        getRates(DEFAULT_RATE)
        subscribeToViewEvents()
    }

    fun detachView() {
        disposables.clear()
    }

    private fun subscribeToViewEvents() {
        ratesView?.viewEvents()
            ?.distinctUntilChanged()
            ?.subscribe { handleViewEvent(it) }
            ?.addTo(disposables)
    }

    private fun handleViewEvent(viewEvent: ViewEvent) {
        when (viewEvent) {
            is ViewEvent.ActiveRateInput -> valueSubject.onNext(Value(viewEvent.value))
            is ViewEvent.RateSelection -> getRates(viewEvent.rate, viewEvent.value)
        }
    }

    private fun getRates(rate: String, firstValue: BigDecimal? = null) {
        ratesInteractor.rates(rate, valueSubject, firstValue)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ ratesView?.showRates(it, rate) }, { ratesView?.showError() })
            .addTo(disposables)
    }

    private companion object {
        const val DEFAULT_RATE = "EUR"
    }
}

internal interface RatesView {

    fun showError()

    fun showRates(rates: List<Rate>, editableRateName: String)

    fun viewEvents(): Observable<out ViewEvent>
}

internal sealed class ViewEvent {

    data class ActiveRateInput(val rate: String, val value: BigDecimal?) : ViewEvent()

    data class RateSelection(val rate: String, val value: BigDecimal?) : ViewEvent()
}