package pl.whiter13.revolut.di

import com.squareup.moshi.Moshi
import com.squareup.moshi.adapters.Rfc3339DateJsonAdapter
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import dagger.Module
import dagger.Provides
import pl.whiter13.revolut.utils.BigDecimalAdapter
import java.util.*
import javax.inject.Singleton

@Module
internal class SerializationModule {

    @Provides
    @Singleton
    fun provideMoshi(): Moshi {
        return Moshi.Builder()
            .add(Date::class.java, Rfc3339DateJsonAdapter())
            .add(BigDecimalAdapter())
            .add(KotlinJsonAdapterFactory())
            .build()
    }
}