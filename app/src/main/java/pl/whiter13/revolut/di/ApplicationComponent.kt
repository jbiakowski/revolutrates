package pl.whiter13.revolut.di

import dagger.Component
import pl.whiter13.revolut.rates.RatesComponent
import pl.whiter13.revolut.rates.RatesModule
import javax.inject.Singleton

@Singleton
@Component(modules = [SerializationModule::class, NetworkModule::class])
internal interface ApplicationComponent {

    fun plus(ratesModule: RatesModule): RatesComponent
}