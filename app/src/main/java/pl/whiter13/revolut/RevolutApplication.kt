package pl.whiter13.revolut

import android.app.Application
import android.content.Context
import com.facebook.stetho.Stetho
import pl.whiter13.revolut.di.ApplicationComponent
import pl.whiter13.revolut.di.DaggerApplicationComponent

internal class RevolutApplication : Application() {

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        applicationComponent = DaggerApplicationComponent.create()
    }

    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) {
            Stetho.initializeWithDefaults(this)
        }
    }

    companion object {
        lateinit var applicationComponent: ApplicationComponent
    }
}