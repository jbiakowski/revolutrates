package pl.whiter13.revolut.utils

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import java.math.BigDecimal

internal class BigDecimalAdapter {

    @Suppress("unused") //used by moshi reflection
    @FromJson
    fun fromJson(value: String): BigDecimal = value.toBigDecimal()

    @Suppress("unused") //used by moshi reflection
    @ToJson
    fun toJson(value: BigDecimal): String = value.toPlainString()
}