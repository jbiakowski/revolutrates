plugins {
    id("com.android.application")
    id("kotlin-android")
    id("kotlin-android-extensions")
    id("kotlin-kapt")
}

android {
    compileSdkVersion(28)
    defaultConfig {
        applicationId = "pl.whiter13.revolut"
        minSdkVersion(21)
        targetSdkVersion(28)
        versionCode = 1
        versionName = "1.0"
        testInstrumentationRunner = "android.support.test.runner.AndroidJUnitRunner"
    }
    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android.txt"), file("proguard-rules.pro"))
        }
    }

    compileOptions {
        setSourceCompatibility(JavaVersion.VERSION_1_8)
        setTargetCompatibility(JavaVersion.VERSION_1_8)
    }

    packagingOptions {
        exclude("META-INF/proguard/androidx-annotations.pro")
    }
}

dependencies {
    implementation(Dependencies.KOTLIN)
    implementation(Dependencies.APPCOMPAT)
    implementation(Dependencies.RECYCLER_VIEW)

    implementation(Dependencies.DAGGER)
    kapt(Dependencies.DAGGER_COMPILER)

    implementation(Dependencies.OK_HTTP)
    implementation(Dependencies.MOSHI)
    implementation(Dependencies.MOSHI_ADAPTERS)
    implementation(Dependencies.MOSHI_KOTLIN)
    implementation(Dependencies.RETROFIT)
    implementation(Dependencies.RETROFIT_RXJAVA)
    implementation(Dependencies.RETROFIT_MOSHI)

    implementation(Dependencies.RX_JAVA)
    implementation(Dependencies.RX_ANDROID)
    implementation(Dependencies.RX_KOTLIN)
    implementation(Dependencies.RX_BINDING)

    implementation(Dependencies.STETHO)
    implementation(Dependencies.STETHO_OK_HTTP)
}
